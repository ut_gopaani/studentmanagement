from .models import *
from django.contrib.auth.models import User
from rest_framework import serializers

class StudentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Student
		fields = '__all__'

class TeacherSerializer(serializers.ModelSerializer):
	class Meta:
		model = Teacher
		fields = "__all__"

class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='user_profile:userprofile-detail')
	user = serializers.HyperlinkedIdentityField(view_name='user_profile:user-detail')
	class Meta:
		model = UserProfile
		fields = ['url', 'id', 'user', 'phone_number', 'dob', 'address', 'user_type' ]

class UserSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='user_profile:user-detail')
	class Meta:
		model = User
		fields = ['url', 'id', 'first_name', 'last_name', 'username', 'email']