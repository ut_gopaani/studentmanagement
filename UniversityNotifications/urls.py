from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views

from user_profile.urls import router

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', include('user_profile.urls')),
]
